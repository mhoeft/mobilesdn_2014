#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDateTime>
#include <QRegExp>
#include <QFile>
#include <QMessageBox>

#define DEBUG_ARP   0
#define DEBUG_HELLO 0
#define DEBUG_JSON  0

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    sock = new QUdpSocket(this);
    connect(sock, SIGNAL(readyRead()), SLOT(readUDP()));
    connect(ui->master, SIGNAL(clicked()), SLOT(sourceChanged()));
    connect(ui->slave, SIGNAL(clicked()), SLOT(sourceChanged()));
    connect(ui->jsonBtn, SIGNAL(clicked()), SLOT(resendJSON()));
    sock->bind(60000, QUdpSocket::ShareAddress);

    startTimer(5000);
}

void MainWindow::resendJSON()
{
    if (jsonClients.isEmpty() || jsonServer.isEmpty()) {
        QMessageBox::warning(this, "JSON not ready",
                             "One of JSONs is not formed yet", QMessageBox::Ok);
        return;
    }

    sendString(ui->ctrlAddr->text(), ui->ctrlPort->value(), jsonServer);
    sendString(ui->ctrlAddr->text(), ui->ctrlPort->value(), jsonClients);
}

void MainWindow::readArp()
{
#define BUF_SZ 1024
    QFile file("/proc/net/arp");
    if (!file.exists()) {
        qCritical() << file.fileName() << "does not exits";
        return;
    }
    file.open(QIODevice::ReadOnly);
    if (!file.isOpen()) {
        qCritical() << "Could not open" << file.fileName() << file.errorString();
        return;
    }

    char data[BUF_SZ];

    while (file.readLine(data, BUF_SZ) > 0) {
        QString line(data);
        QRegExp ip("(\\d{1,3}.){3}\\d{1,3}");
        QRegExp mac("([0-9a-fA-F]{2}:){5}([0-9a-fA-F]{2})");

        ip.indexIn(line);
        mac.indexIn(line);

        if (ip.capturedTexts().first().isEmpty() ||
            mac.capturedTexts().first() == "00:00:00:00:00:00") {
#if DEBUG_ARP
            qDebug() << "{arp} skip" << ip.capturedTexts() << mac.capturedTexts();
#endif
        } else {
#if DEBUG_ARP
            qDebug() << "{arp} Saving valid MAC address to the map" << ip.capturedTexts().first() << mac.capturedTexts().first();
#endif
            macs[ip.capturedTexts().first()] = mac.capturedTexts().first();
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sendString(QString host, int port, QString msg)
{
    /* Send JSON to controller */
    QUdpSocket sock(this);
    QHostAddress serv(host);

    if (serv.isNull() || serv.toString() != host) {
        qCritical() << "Wrong IP";
        return;
    }

    sock.connectToHost(serv, port);
    sock.waitForConnected();

    QByteArray datagram(msg.toAscii());
    sock.write(datagram);
    sock.waitForBytesWritten();
    sock.close();
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    event->accept();

    readArp();

    QStringList validated;
    QMutableListIterator<QString> i(newAddresses);
    while (i.hasNext()) {
        QString addr = i.next();
        if (!macs.contains(addr)) {
            qWarning() << "MAC address of client" << addr << "is unknown, skipping";
            i.remove();
            continue;
        }
        validated.append("{ \"ip\" : \"" + addr + "\", \"mac\" : \"" + macs[addr] + "\" }");
    }

    bool changed = (addresses.size() != newAddresses.size());

    if (addresses.size() == newAddresses.size()) {
        QString addr;

        foreach (addr, addresses) {
            if (! newAddresses.contains(addr))
                changed = true;
        }
    }

    /* newAddresses list have to be cleared on every tick,
     * for us to know when client times out. */
    addresses = newAddresses;
    //newAddresses.clear();

    if (!changed)
        return;

    qDebug () << "Client list changed, sending update to Floodlight";

    ui->listWidget->clear();
    ui->listWidget->addItems(addresses);

    /* Send JSON to controller */
    jsonServer = QString("{"
                         "\"type\": \"fullUpdate\","
                         "\"groupIP\": \"230.0.0.255\","
                         "\"data\": [");
    jsonServer.append(validated.join(", "));
    jsonServer.append("]"
                "}");
#if DEBUG_JSON
    qDebug() << "{json}" << json;
#endif
    sendString(ui->ctrlAddr->text(), ui->ctrlPort->value(), jsonServer);
}

#define GSTREAMER_PORT 60001

void MainWindow::sourceChanged()
{
    QString currentHost, newHost;

    if (QObject::sender() == oldSource)
        return;

    if (QObject::sender() == ui->master) {
        qDebug() << "Switching to master!";

        newHost = ui->masterIP->text();
        currentHost = ui->slaveIP->text();
    } else {
        qDebug() << "Switching to slave!";

        currentHost = ui->masterIP->text();
        newHost = ui->slaveIP->text();
    }
    oldSource = QObject::sender();

    qint64 changeTime = QDateTime::currentMSecsSinceEpoch() / 1000 + 2;

    sendString(newHost, GSTREAMER_PORT, QString("s %1").arg(changeTime));
    sendString(currentHost, GSTREAMER_PORT, QString("e %1").arg(changeTime));

    readArp();

    if (!macs.contains(newHost)) {
        qCritical() << "MAC address of new server is unknown!" << newHost;
        return;
    }

    /* Notify SDN controller about source change */
    jsonClients = QString("{"
                     "\"type\": \"serverSwitch\","
                     "\"data\": [{ \"ip\" : \"");
    jsonClients.append(newHost + "\", \"mac\" : \"" + macs[newHost]);
    jsonClients.append("\" }]"
                "}");
#if DEBUG_JSON
    qDebug() << "{json}" << json;
#endif
    sendString(ui->ctrlAddr->text(), ui->ctrlPort->value(), jsonClients);
}

void MainWindow::readUDP()
{
    const int buf_len = 1500;
    char buf[buf_len];
    QHostAddress peer;

    while (sock->hasPendingDatagrams()) {
        sock->readDatagram(buf, buf_len, &peer);

        if (!newAddresses.contains(peer.toString())) {
#if DEBUG_HELLO
            qDebug() << "{hello} new hello from:" << peer;
#endif
            newAddresses.append(peer.toString());
        }
    }
}
