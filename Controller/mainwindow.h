#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMap>
#include <QMainWindow>
#include <QUdpSocket>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void timerEvent(QTimerEvent *event);
    
private:
    void sendString(QString host, int port, QString msg);

private slots:
    void readUDP();
    void sourceChanged();
    void readArp();
    void resendJSON();

private:
    Ui::MainWindow *ui;
    QString jsonClients, jsonServer;
    QStringList addresses, newAddresses;
    QUdpSocket *sock;
    QObject *oldSource;
    QMap<QString, QString> macs;
};

#endif // MAINWINDOW_H
