package net.floodlightcontroller.multicast;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFPacketIn;
import org.openflow.protocol.OFPacketOut;
import org.openflow.protocol.OFType;
import org.openflow.protocol.action.OFAction;
import org.openflow.protocol.action.OFActionDataLayerDestination;
import org.openflow.protocol.action.OFActionDataLayerSource;
import org.openflow.protocol.action.OFActionNetworkLayerDestination;
import org.openflow.protocol.action.OFActionOutput;
import org.openflow.util.HexString;
import org.openflow.util.U16;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.IListener.Command;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.core.util.AppCookie;

import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.devicemanager.IDevice;
import net.floodlightcontroller.devicemanager.IDeviceService;
import net.floodlightcontroller.devicemanager.SwitchPort;
import net.floodlightcontroller.forwarding.Forwarding;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.linkdiscovery.LinkInfo;
import net.floodlightcontroller.linkdiscovery.LinkTuple;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.routing.ForwardingBase;
import net.floodlightcontroller.routing.IRoutingDecision;
import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.routing.Route;
import net.floodlightcontroller.topology.TopologyManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.net.InetAddresses;

public class MulticastForwarding extends Forwarding implements 
		IFloodlightModule {
	
	public static MulticastForwarding multicastForwarding = null;
	protected IFloodlightProviderService floodlightProvider;
	protected static Logger log;
	protected ILinkDiscoveryService linkDiscovery;
	protected IDeviceService deviceManager;

	@Override
	public String getName() {
	    return MulticastForwarding.class.getSimpleName();
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
	    Collection<Class<? extends IFloodlightService>> l =
	        new ArrayList<Class<? extends IFloodlightService>>();
	    l.add(IFloodlightProviderService.class);
	    return l;
	}

	@Override
	public void init(FloodlightModuleContext context) throws FloodlightModuleException {
		multicastForwarding = this;
	    floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
	    linkDiscovery = context.getServiceImpl(ILinkDiscoveryService.class);
	    deviceManager = context.getServiceImpl(IDeviceService.class);
	    log = LoggerFactory.getLogger(MulticastForwarding.class);
	    
	    
	}

	@Override
	public void startUp(FloodlightModuleContext context) {
	    floodlightProvider.addOFMessageListener(OFType.PACKET_IN, this);
	    ReceiverUpdateServer rus = new ReceiverUpdateServer();
        rus.run();
	}

	
	
    @Override
    public Command processPacketInMessage(IOFSwitch sw, OFPacketIn pi, IRoutingDecision decision, FloodlightContext cntx) {
        Ethernet eth = IFloodlightProviderService.bcStore.get(cntx, 
                                                       IFloodlightProviderService.CONTEXT_PI_PAYLOAD);
        if (eth.getEtherType() == Ethernet.TYPE_IPv4 && isMulticast((IPv4)eth.getPayload())){
        	doMulticast(sw, pi, cntx);
        }
        
        return Command.CONTINUE;
    }
	
    
    private Boolean isMulticast(IPv4 ipPkt) {
    	int dest = ipPkt.getDestinationAddress();
    	InetAddress address = InetAddresses.fromInteger(dest);
    	return address.isMulticastAddress();
    	//Tak mozna dostac stringa
//    	String ipStr = 
//    			  String.format("%d.%d.%d.%d",
//    					 (dest >> 24 & 0xff),
//    					 (dest >> 16 & 0xff),  
//    			         (dest >> 8 & 0xff),             
//    			         (dest & 0xff));
    }
    
public void topologyChangeListener() {
	
}
    

public void writeReverseFlow(Vertex vertex, OutObject outObject) {
	try {
		if (vertex.getInPort() == outObject.getOutPort() || outObject.getOutPort() == 0 || vertex.getInPort() == 0) {
			System.err.println("Pomijam bledny port dla reverse vertexa: " + vertex);
			return;
		}
		IOFSwitch swi = vertex.sw;
		
		OFMatch reverseMatch = new OFMatch();
		reverseMatch.setInputPort(outObject.getOutPort());
		//TODO: vertex powinien zawierac MACa
		//reverseMatch.setDataLayerDestination();
		int reverseWildcards = ((Integer)swi.getAttribute(IOFSwitch.PROP_FASTWILDCARDS)).intValue() &
	            ~OFMatch.OFPFW_IN_PORT; //&
	            //~OFMatch.OFPFW_DL_DST;
		reverseMatch.setWildcards(reverseWildcards);
		List<OFAction> reverseActionList = new ArrayList<OFAction>();
		
		OFFlowMod reverseFlow = new OFFlowMod();
		reverseFlow.setCookie(666);
		reverseFlow.setPriority((short) 201); 
		reverseFlow.setMatch(reverseMatch);
		//TODO: Ustawic krotszy timeout po testach
		reverseFlow.setIdleTimeout((short) 300);
		//TODO: Tutaj moze ustawiac jakis ID?
		reverseFlow.setBufferId(OFPacketOut.BUFFER_ID_NONE);
		
		OFActionOutput outputAction = new OFActionOutput();
		short outPort = vertex.getInPort();
		
		outputAction.setPort(outPort);
		
		outputAction.setLength((short) OFActionOutput.MINIMUM_LENGTH);
		
		reverseActionList.add(outputAction);
		
		reverseFlow.setActions(reverseActionList);
		
		LinkedList<OFMessage> li = new LinkedList<OFMessage>();
		
		int flowLength = OFFlowMod.MINIMUM_LENGTH;
		for (OFAction action : reverseActionList) {
			if (action instanceof OFActionDataLayerDestination) {
				flowLength+=OFActionDataLayerDestination.MINIMUM_LENGTH;
			} else if (action instanceof OFActionNetworkLayerDestination) {
				flowLength+=OFActionNetworkLayerDestination.MINIMUM_LENGTH;
			} else if (action instanceof OFActionDataLayerDestination) {
					flowLength+=OFActionDataLayerDestination.MINIMUM_LENGTH;
			} else if (action instanceof OFActionOutput) {
				flowLength+=OFActionOutput.MINIMUM_LENGTH;
			}
		}
		reverseFlow.setLength(U16.t(flowLength));
		
	    li.add(reverseFlow);
	    if (reverseActionList.size() > 0) {
	        log.info("Setting reverse flow on a switch " + swi.getId());
	        swi.write(li, null);
	    } else {
	    	log.info("No actions, skipping write for switch " + swi.getId());
	    }
	} catch (Exception ex) {
		log.info("Error while writing reverse flow: " + ex.toString());
	}
}
    
private void rebuildMulticastFlows() {
	System.err.println("Budowanie drzewa (internal)");
	int multicastIP=1;
	String defaultMulticastGroup="230.0.0.250";
	String defaultClientIp = "10.255.255.255";
	try {
		multicastIP = InetAddresses.coerceToInteger(InetAddress.getByName(defaultMulticastGroup));
	} catch (Exception ex) {
		log.info("zlee: " + ex.toString());
	}
		
	try {
			MulticastService.computeTopology(floodlightProvider, linkDiscovery, deviceManager, "PPH");
			List<Vertex> vertices = MulticastService.getSwMulticastInfo();
			for (Vertex vertex : vertices) {
				
				
				IOFSwitch swi = vertex.sw;
				List<OutObject> switches = new ArrayList<OutObject>();
				List<OutObject> clients = new ArrayList<OutObject>();
				
				OFMatch match = new OFMatch();
				match.setInputPort(vertex.inPort);
		    	match.setNetworkDestination(multicastIP);
		    	match.setDataLayerType((short)0x0800);
		    	int wildcards = ((Integer)swi.getAttribute(IOFSwitch.PROP_FASTWILDCARDS)).intValue() &
		                ~OFMatch.OFPFW_IN_PORT & 
		                ~OFMatch.OFPFW_DL_TYPE &
		                ~OFMatch.OFPFW_NW_DST_MASK;
		    	match.setWildcards(wildcards);
		    	List<OFAction> actionList = new ArrayList<OFAction>();
		    	
		    	OFFlowMod flow = new OFFlowMod();
		    	flow.setCookie(666);
		    	flow.setPriority((short) 200); 
		    	flow.setMatch(match);
		    	//TODO: Ustawic krotszy timeout po testach
		    	flow.setIdleTimeout((short) 300);
		    	//TODO: Tutaj moze ustawiac jakis ID?
		    	flow.setBufferId(OFPacketOut.BUFFER_ID_NONE);
		    	LinkedList<OFMessage> li = new LinkedList<OFMessage>();
				for (OutObject o : vertex.getOutPorts()) {
					if (vertex.getInPort() == o.getOutPort() || o.getOutPort() == 0 || vertex.getInPort() == 0) {
						System.err.println("Pomijam bledny port dla forward vertexa: " + vertex);
						continue;
					}
					if (!o.isUser()) {
						switches.add(o);
					} else {
						clients.add(o);
					}
				}
				for (OutObject o : switches) {
					OFActionOutput outputAction = new OFActionOutput();
					short outPort = o.getOutPort();
					
					outputAction.setPort(outPort);
					
					outputAction.setLength((short) OFActionOutput.MINIMUM_LENGTH);
					
					actionList.add(outputAction);
					
					//TODO: Zakomentowane dla testow, moze zwykly Forwarding ustawi reverse flow
					//writeReverseFlow(vertex, o);
				}
				for (OutObject o : clients) {
					OFActionDataLayerDestination macAction = new OFActionDataLayerDestination();
					OFActionNetworkLayerDestination ipAction = new OFActionNetworkLayerDestination();
					OFActionOutput outputAction = new OFActionOutput();
					
					byte[] clientMac = HexString.fromHexString(o.getMacAddress());
					//TODO: czy to sie nie sypnie z powodu indeksu
					int clientIP = InetAddresses.coerceToInteger(InetAddress.getByName(defaultClientIp));
					try {
						clientIP = o.getIpv4Address()[0];
					} catch (Exception ex) {
						log.info("Using default client ip: " + defaultClientIp);
					}
					short outPort = o.getOutPort();
					
					macAction.setDataLayerAddress(clientMac);
					ipAction.setNetworkAddress(clientIP);
					outputAction.setPort(outPort);
					
					macAction.setLength((short) OFActionDataLayerDestination.MINIMUM_LENGTH);
					ipAction.setLength((short) OFActionNetworkLayerDestination.MINIMUM_LENGTH);
					outputAction.setLength((short) OFActionOutput.MINIMUM_LENGTH);
					
					actionList.add(macAction);
					actionList.add(ipAction);
					actionList.add(outputAction);
					
					//TODO: Zakomentowane dla testow, moze zwykly Forwarding ustawi reverse flow
					//writeReverseFlow(vertex, o);
				}
				flow.setActions(actionList);
				int flowLength = OFFlowMod.MINIMUM_LENGTH;
				for (OFAction action : actionList) {
					if (action instanceof OFActionDataLayerDestination) {
						flowLength+=OFActionDataLayerDestination.MINIMUM_LENGTH;
					} else if (action instanceof OFActionNetworkLayerDestination) {
						flowLength+=OFActionNetworkLayerDestination.MINIMUM_LENGTH;
					} else if (action instanceof OFActionOutput) {
						flowLength+=OFActionOutput.MINIMUM_LENGTH;
					}
				}
				flow.setLength(U16.t(flowLength));
				
		        li.add(flow);
		        if (actionList.size() > 0) {
			        log.info("Setting flow on a switch " + swi.getId());
			        swi.write(li, null);
		        } else {
		        	log.info("No actions, skipping write for switch " + swi.getId());
		        }
			}
		}
		catch (Exception ex) {
			log.info("Error while writing forward flow: " + ex.toString());
		}
}

public void rebuildMulticastFlows(String reason) {
	System.err.println("Wywolano przebudowanie drzewa: " + reason);
	clearFlows();
	rebuildMulticastFlows();
	//doRebuild=true;
}
    
private boolean doRebuild = true;

protected void doMulticast(IOFSwitch sw, OFPacketIn pi, FloodlightContext cntx) {
	try {
		if (doRebuild == true) {
			doRebuild = false;
			rebuildMulticastFlows();
		}
	}
	catch (Exception ex) {
		System.err.println("Blad w doMulticast: " + ex.toString());
	}
}

	public void clearFlows() {
		log.info("clearFlows()");
		Map<Long, IOFSwitch> switches = new HashMap<Long, IOFSwitch>(floodlightProvider.getSwitches());
		for (Map.Entry<Long, IOFSwitch> entry : switches.entrySet()) {
			IOFSwitch s = entry.getValue();
			if (s.getId() < 100) {
				log.info("Sending clearFlowMod to switch " + s.getId());
				s.clearAllFlowMods();
			}
		}
		
        
	}
}
