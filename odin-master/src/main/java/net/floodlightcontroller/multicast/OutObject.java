package net.floodlightcontroller.multicast;

public class OutObject {

	public boolean isUser;
	public String macAddress;
	public Integer[] ipv4Address;
	public short outPort;

	public OutObject(boolean isUser, short outPort) {
		this.isUser = isUser;
		this.outPort = outPort;
	}

	public boolean isUser() {
		return isUser;
	}

	public void setUser(boolean isUser) {
		this.isUser = isUser;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public Integer[] getIpv4Address() {
		return ipv4Address;
	}

	public void setIpv4Address(Integer[] ipv4Address) {
		this.ipv4Address = ipv4Address;
	}

	public short getOutPort() {
		return outPort;
	}

	public void setOutPort(short outPort) {
		this.outPort = outPort;
	}
	
	
	
}
