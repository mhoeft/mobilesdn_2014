package net.floodlightcontroller.multicast;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openflow.protocol.OFPhysicalPort;

import com.google.common.net.InetAddresses;

import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.devicemanager.IDevice;
import net.floodlightcontroller.devicemanager.SwitchPort;
import net.floodlightcontroller.linkdiscovery.LinkTuple;
import net.floodlightcontroller.linkdiscovery.LinkInfo;
import net.floodlightcontroller.topology.TopologyManager;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.multicast.ReceiverUpdate.IpMacPair;
import net.floodlightcontroller.devicemanager.IDeviceService;

public class MulticastService {

//	public class SwitchPortsPair {
//		private IOFSwitch sw;
//		IDevice[] outDevices;
//		short portIn;
//		
//		public SwitchPortsPair() {
//			
//		}
//		
//		public IOFSwitch getSwitch() {
//			return sw;
//		}
//		public void setSwitch(IOFSwitch sw) {
//			this.sw = sw;
//		}
//		public IDevice[] getOutDevices() {
//			return outDevices;
//		}
//		public void setOutDevices(IDevice[] outDevices) {
//			this.outDevices = outDevices;
//		}
//		public short getPortIn() {
//			return portIn;
//		}
//		public void setPortIn(short portIn) {
//			this.portIn = portIn;
//		}
//	}
	
	public static String handoffClientMAC;
	public static long handoffSwID;
	
	public static void saveHandoffClientMAC(String clientMAC) {
		handoffClientMAC = clientMAC;
	}
	
	public static void saveHandoffSwID(long switchID) {
		System.err.println("Zapisano switchID " + switchID);
		handoffSwID = switchID;
	}
	
	public static String currentServerMAC = "1c:6f:65:c1:e1:c8";
	public static List<IpMacPair> currentReceivers = new ArrayList<IpMacPair>();
	
	public static void saveMAC(List<IpMacPair> ipMac) {
		if(ipMac.size() > 0) {
			currentServerMAC = ipMac.get(0).getMac();
			System.out.println("Ustawiam MAC serwera na " + currentServerMAC);
		}
		else {
			System.err.println("Nieznany MAC. Ustawiam domyslny.");
			currentServerMAC = "00:00:00:00:00:00";
		}
	}
	
	public static void saveReceivers(List<IpMacPair> receivers) {
		currentReceivers = receivers;
	}
	
	private static List<Vertex> ports;
	
	public static void computeTopology(IFloodlightProviderService floodlightProvider, ILinkDiscoveryService linkDiscovery, IDeviceService deviceManager, String multicastAlgorithm){
		
		Map<Long, IOFSwitch> switches = new HashMap<Long, IOFSwitch>(floodlightProvider.getSwitches());

    	Map<LinkTuple, LinkInfo> myLinks;	
    	myLinks = new HashMap<LinkTuple, LinkInfo>(linkDiscovery.getLinks());
    	Set<LinkTuple> linksBetweenSw = new HashSet<LinkTuple>(myLinks.keySet());	
    	
    	ArrayList<IDevice> myDevices;
    	myDevices = new ArrayList<IDevice>(deviceManager.getAllDevices());
    	
    	
    	List<Vertex> vertices = new ArrayList<Vertex>(); // all vertices
        List<Vertex> receivers = new ArrayList<Vertex>(); // only hosts
        List<Vertex> swList = new ArrayList<Vertex>(); // only switches
        Vertex srcVertex = new Vertex();
		Vertex dstVertex = new Vertex();
        
        for(IOFSwitch swi : switches.values()){
        	swList.add(new Vertex(Long.toString(swi.getId()), swi)); // add switches
        }
        
        for(Iterator<LinkTuple> iter = linksBetweenSw.iterator(); iter.hasNext(); ){
    		LinkTuple link = iter.next();
    		
    		int vertexIndex = 0;
    		
    		for(int i=0; i<swList.size(); i++){
    			if(swList.get(i).name.equals(Long.toString(link.getSrc().getSw().getId()))){
    				srcVertex = swList.get(i);
    				vertexIndex = i;
    			}
    			if(swList.get(i).name.equals(Long.toString(link.getDst().getSw().getId()))){
    				dstVertex = swList.get(i);
    			}
    		}
    		
    		swList.get(vertexIndex).adjacencies.add(new Edge(srcVertex, dstVertex, 1));
    		
    	}
        
        vertices.add(new Vertex(currentServerMAC));

        for(IDevice iDev : myDevices){
        	long currentSwitchDPID = 0;
        	if(iDev.getAttachmentPoints().length > 0) {
        		currentSwitchDPID = iDev.getAttachmentPoints()[0].getSwitchDPID();
        	}
       
        	if (handoffClientMAC != null && iDev.getMACAddressString() != null && iDev.getAttachmentPoints().length > 0) {
	        	if (iDev.getMACAddressString().toLowerCase().equals(handoffClientMAC.toLowerCase())) {
	        		System.err.println("Obecny punkt przylaczenia dla " + iDev.getMACAddress() + " to " + currentSwitchDPID);
	        		currentSwitchDPID = handoffSwID;
	        		System.err.println("Nowy punkt przylaczenia dla " + iDev.getMACAddress() + " to " + currentSwitchDPID);
	        	}
        	}
        	
        	if (iDev.getMACAddressString().equals(currentServerMAC)) {
        		for(int i=0; i<swList.size(); i++){       			
        			if(swList.get(i).name.equals(Long.toString(currentSwitchDPID))) {
        				vertices.get(0).adjacencies.add(new Edge(vertices.get(0), swList.get(i), 1));
        		        swList.get(i).adjacencies.add(new Edge(swList.get(i), vertices.get(0), 1));
        			}
        		}
        	}
        	else {
        		Vertex tmpVert = new Vertex(iDev.getMACAddressString());
        		
        		for(IpMacPair ipMac : currentReceivers) {
        			String macAddress = ipMac.getMac();
        			if (iDev.getMACAddressString().equals(macAddress)) {
        				int iter = 0;
                		for(int i=0; i<swList.size(); i++){       			
                			if(swList.get(i).name.equals(Long.toString(currentSwitchDPID))){
                				dstVertex = swList.get(i);
                				iter = i;
                			}
                		}
                		swList.get(iter).adjacencies.add(new Edge(swList.get(iter), tmpVert, 1));
                		tmpVert.adjacencies.add(new Edge(tmpVert, dstVertex, 1));
                		
                		vertices.add(tmpVert);
        			}
        		}	
        	}	
        }
        if (vertices.get(0).adjacencies.size() == 0) {
        	throw new IllegalArgumentException("Serwer nie ma polaczen");
        }
        vertices.addAll(swList);
        
        receivers.addAll(vertices.subList(1, currentReceivers.size()+1));      
    	
        Vertex nextVertex = vertices.get(0);
        
        ports = new ArrayList<Vertex>();
        Iterable<Edge> edgesList = new ArrayList<Edge>();
        
        if (multicastAlgorithm.equals("PPH")) {
        	PPH newPPH = new PPH(vertices, receivers, nextVertex);
        	edgesList = newPPH.edges();
        }
        else if (multicastAlgorithm.equals("STP")) {
        	STP newSTP = new STP(vertices, receivers, nextVertex);
        	edgesList = newSTP.edges();     	
        }
        System.err.println("Drzewo :O");
        for (Edge edge : edgesList) {
    		System.out.println(edge.source + " " + edge.target);
    	}
//    	System.out.println("Suma wag: " + newPPH.getWeight());	// print weight of tree from PPH
        
    	for (Vertex v : swList) {
    		Vertex vertexToAdd = v;
    		boolean isVertexInTree = false;

			for (Edge edge : edgesList) {
				if (v.name.equals(edge.source.name)) {
					for(Iterator<LinkTuple> iter = linksBetweenSw.iterator(); iter.hasNext(); ){
	            		LinkTuple link = iter.next();
	            		if (edge.source.name.equals(Long.toString(link.getSrc().getSw().getId())) && edge.target.name.equals(Long.toString(link.getDst().getSw().getId()))) {
	            			vertexToAdd.outPorts.add(new OutObject(false, link.getSrc().getPort()));
	            			isVertexInTree = true;
	            		}
	        		}
				}
				else if (v.name.equals(edge.target.name)) {
					for(Iterator<LinkTuple> iter = linksBetweenSw.iterator(); iter.hasNext(); ){
	            		LinkTuple link = iter.next();
	            		if (edge.source.name.equals(Long.toString(link.getSrc().getSw().getId())) && edge.target.name.equals(Long.toString(link.getDst().getSw().getId()))) {
	            			vertexToAdd.setInPort(link.getDst().getPort());
	            			isVertexInTree = true;
	            		}
	        		}
				}
        	}
			for(IDevice iDev : myDevices) {
				if (iDev.getAttachmentPoints().length > 0) {
					
					for(IpMacPair ipMac : currentReceivers) {
	        			String macAddress = ipMac.getMac();
	        			if (iDev.getMACAddressString().equals(macAddress)) {
					
							long currentSwitchDPID = iDev.getAttachmentPoints()[0].getSwitchDPID();
				        	if (handoffClientMAC != null && iDev.getMACAddressString() != null && iDev.getAttachmentPoints().length > 0) {
					        	if (iDev.getMACAddressString().toLowerCase().equals(handoffClientMAC.toLowerCase())) {
					        		currentSwitchDPID = handoffSwID;
					        	}
				        	}
							
			    			if (v.name.equals(Long.toString(currentSwitchDPID))) {
			    				OutObject outObj = new OutObject(true, (short)iDev.getAttachmentPoints()[0].getPort());
			    				outObj.setMacAddress(iDev.getMACAddressString());
			    				outObj.setIpv4Address(iDev.getIPv4Addresses());
			    				vertexToAdd.outPorts.add(outObj);
			    				isVertexInTree = true;
			    			} 
	        			}
	        			if (v.name.equals(vertices.get(0).adjacencies.get(0).target.name) && iDev.getMACAddressString().equals(currentServerMAC)) {
		    				vertexToAdd.inPort = (short)iDev.getAttachmentPoints()[0].getPort();
		    				isVertexInTree = true;
		    			}
					}
				}
        	}
			
			if (isVertexInTree) {
				ports.add(vertexToAdd);
			}
    	}
    	
//    	System.out.println("-------------------------------- START");
//    	for(Vertex vout : ports) {
//    		System.out.println("-------------------- START Lista");
//			System.out.println("Name: " + vout.name +  " Name2: " + vout.sw.getId() + " + inPort: " + vout.inPort);
//			for (OutObject obj : vout.outPorts) {
//				System.out.println("isUser: " + obj.isUser);
//				System.out.println("MAC: " + obj.getMacAddress());
//				System.out.println("IP: " + obj.getIpv4Address());
//				System.out.println("outPort: " + obj.getOutPort());
//				System.out.println("----------------------------");
//			}
//			System.out.println("-------------------- STOP Lista");
//    	}
//    	System.out.println("-------------------------------- STOP");
       
	}
	
	public static List<Vertex> getSwMulticastInfo() {
		if (currentReceivers.isEmpty()) {
			return Collections.<Vertex>emptyList();
		}
		
		return ports;
	}
	
//	public static List<SwitchPortsPair> computePath(IOFSwitch sw, Collection<IDevice> subscribers) {
//		
//		
//		
//		return Collections.emptyList();
//	}
}
