package net.floodlightcontroller.multicast;

import java.util.List;

public class ReceiverUpdate {
	
	public class IpMacPair {
		private String ip;
		private String mac;
		
		public String getIp() {
			return ip;
		}
		public void setIp(String ip) {
			this.ip = ip;
		}
		public String getMac() {
			return mac;
		}
		public void setMac(String mac) {
			this.mac = mac;
		}
		
		@Override
		public String toString() {
			return String.format("IP:%s, MAC:%s", ip, mac);
		}
	}
	
	private String type;
	private String groupIP;
	private List<IpMacPair> data;
	
	public String getType() { return type; }
	public String getGroupIP() { return groupIP; }
	public List<IpMacPair> getData() { return data; }
	
	public void setType(String type) {
		this.type = type;
	}
	public void setGroupIP(String groupIP) {
		this.groupIP = groupIP;
	}
	public void setData(List<IpMacPair> data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return String.format("type:%s,groupIP:%s,data:%s", type, groupIP, data);
	}
}
