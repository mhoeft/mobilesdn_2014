package net.floodlightcontroller.multicast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

import org.easymock.internal.matchers.CompareEqual;

public class STP {

	private int receiversCount;
	private Vertex bestVertex;   
    private List<Edge> multicastEdges;
	
	public STP(List<Vertex> vertices, List<Vertex> receivers, Vertex nextVertex) {
		
		receiversCount = receivers.size();
		bestVertex = new Vertex();
		multicastEdges = new ArrayList<Edge>();
		
		for(int i=0; i<receiversCount; i++) {          
            bestVertex.minDistance = 100;
            computeBestPaths(nextVertex);
            
            for(Vertex v : vertices.subList(1, receiversCount+1)) {
                if(v.belongToSteiner == false && v.minDistance < bestVertex.minDistance) {
                    nextVertex = v;
                    bestVertex.minDistance = v.minDistance;
                }
            }
            nextVertex.belongToSteiner = true;
            List<Edge> tmpListE = getShortestPathEdge(nextVertex);
            for (Edge e : tmpListE) {
            	if (!e.compareEdges(multicastEdges)) {
            		multicastEdges.add(e);
            	}
            }
            initVertices(vertices);
        }	
	}
	
	private static void computeBestPaths(Vertex source) {
        
        source.minDistance = 0.;
        
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
      	vertexQueue.add(source);

		while(!vertexQueue.isEmpty()) {
	            
		    Vertex nextVertex = vertexQueue.poll();

            for(Edge e : nextVertex.adjacencies) {
                
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughNextVertex = nextVertex.minDistance + weight;
                
		if(distanceThroughNextVertex < v.minDistance) {
		    vertexQueue.remove(v);
		    v.minDistance = distanceThroughNextVertex ;
		    v.previous = nextVertex;
		    vertexQueue.add(v);
		}
            }
        }
    }
    
    private static void initVertices(List<Vertex> vertices) {
        
        for(Vertex v : vertices) {
            v.previous = null;
            v.minDistance = Double.POSITIVE_INFINITY;
        }    
    }
    
    private static List<Edge> getShortestPathEdge(Vertex target) {
        
        List<Edge> edgesPath = new ArrayList<Edge>();
        
        Vertex tmpTarget;
        while(target.previous != null) {
        	tmpTarget = target;
        	target = target.previous;
        	edgesPath.add(new Edge(target, tmpTarget, 1));
        }
        
        Collections.reverse(edgesPath);
        return edgesPath;
    }
    
    public Iterable<Edge> edges() {
    	return multicastEdges;
    }
	
}
