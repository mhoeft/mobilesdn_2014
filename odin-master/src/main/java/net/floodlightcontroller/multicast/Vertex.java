package net.floodlightcontroller.multicast;

import java.util.ArrayList;
import java.util.List;

import net.floodlightcontroller.core.IOFSwitch;

public class Vertex implements Comparable<Vertex> {
    
    public String name;
    public List<Edge> adjacencies = new ArrayList<Edge>();
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public boolean belongToSteiner = false;
    public List<OutObject> outPorts = new ArrayList<OutObject>();
    public short inPort;
    public IOFSwitch sw;
    
    public Vertex() {
    }
    
    public Vertex(String argName) {
        name = argName;
    }
    
    public Vertex(String argName, IOFSwitch argSw) {
    	name = argName;
    	sw = argSw;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Edge> getAdjacencies() {
		return adjacencies;
	}

	public void setAdjacencies(List<Edge> adjacencies) {
		this.adjacencies = adjacencies;
	}

	public double getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }

    public Vertex getPrevious() {
        return previous;
    }

    public void setPrevious(Vertex previous) {
        this.previous = previous;
    }

    public boolean isBelongToSteiner() {
        return belongToSteiner;
    }

    public void setBelongToSteiner(boolean belongToSteiner) {
        this.belongToSteiner = belongToSteiner;
    }
 
    public List<OutObject> getOutPorts() {
		return outPorts;
	}

	public void setOutPorts(List<OutObject> outPorts) {
		this.outPorts = outPorts;
	}

	public short getInPort() {
		return inPort;
	}

	public void setInPort(short inPort) {
		this.inPort = inPort;
	}

	@Override
    public String toString() {
        return name;
    }
    
    @Override
    public int compareTo(Vertex other) {
        return Double.compare(minDistance, other.minDistance);
    }
    
}