package net.floodlightcontroller.multicast;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.DatagramChannelFactory;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.frame.DelimiterBasedFrameDecoder;
import org.jboss.netty.handler.codec.frame.Delimiters;
import org.jboss.netty.handler.codec.string.StringDecoder;
import org.jboss.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReceiverUpdateServer {
	
	protected static final Logger log = LoggerFactory.getLogger(ReceiverUpdateServer.class);
	
	protected int workerThreads = 0;
	protected Config config;

	public void run() {
		try {
			this.config = Config.getConfig();
			//TCP
            //final ServerBootstrap bootstrap = createServerBootStrap();
			//UDP
	        DatagramChannelFactory udpFactory = new NioDatagramChannelFactory(Executors.newCachedThreadPool());
	        ConnectionlessBootstrap bootstrap = new ConnectionlessBootstrap(udpFactory);
            bootstrap.setOption("reuseAddr", true);
            bootstrap.setOption("child.keepAlive", true);
            bootstrap.setOption("child.tcpNoDelay", true);
            bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
            	  public ChannelPipeline getPipeline() throws Exception {
            		  ChannelPipeline pipeline = Channels.pipeline(
                      	    new ReceiverUpdateHandler()
                       	   );
            		  return pipeline;
            	  };
            	 });
            final ChannelGroup cg = new DefaultChannelGroup();
            cg.add(bootstrap.bind(new InetSocketAddress(this.config.getPortNumber())));
            
            log.info("Listening for receiver updates on {}", this.config.getPortNumber());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

//	        // main loop
//	        while (true) {
//	            try {
//	                IUpdate update = updates.take();
//	                update.dispatch();
//	            } catch (InterruptedException e) {
//	                log.error("Received interrupted exception in updates loop;" +
//	                          "terminating process");
//	                terminate();
//	            } catch (StorageException e) {
//	                log.error("Storage exception in controller " +
//	                          "updates loop; terminating process", e);
//	                terminate();
//	            } catch (Exception e) {
//	                log.error("Exception in controller updates loop", e);
//	            }
//	        }
	}
	
    private ServerBootstrap createServerBootStrap() {
        if (workerThreads == 0) {
            return new ServerBootstrap(
                    new NioServerSocketChannelFactory(
                            Executors.newCachedThreadPool(),
                            Executors.newCachedThreadPool()));
        } else {
            return new ServerBootstrap(
                    new NioServerSocketChannelFactory(
                            Executors.newCachedThreadPool(),
                            Executors.newCachedThreadPool(), workerThreads));
        }
    }
    
    static class ReceiverUpdateHandler extends SimpleChannelHandler {
    	 
    	@Override
    	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
    		StringBuilder json = new StringBuilder();
			//TODO: tutaj trzeba zrobic cos calkiem innego
			ChannelBuffer  buf = (ChannelBuffer) e.getMessage();
			while(buf.readable()) {
				json.append((char) buf.readByte());
			}
			MulticastSubscription.parseUpdate(json.toString());
			super.messageReceived(ctx, e);
    	}
    	 
	    @Override
	    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) { 
	        e.getCause().printStackTrace();
	        
	        Channel ch = e.getChannel();
	        ch.close();
	    }
    }
}
