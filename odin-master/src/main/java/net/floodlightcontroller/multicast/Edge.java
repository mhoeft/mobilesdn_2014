package net.floodlightcontroller.multicast;

import java.util.List;

public class Edge implements Comparable<Edge>{
    
    public Vertex source;
    public Vertex target;
    public double weight;
    
    public Edge(Vertex argSource, Vertex argTarget, double argWeight) { 
        source = argSource;
        target = argTarget;
        weight = argWeight;
    }

    public Vertex getSource() {
        return source;
    }

    public void setSource(Vertex source) {
        this.source = source;
    }

    public Vertex getTarget() {
        return target;
    }

    public void setTarget(Vertex target) {
        this.target = target;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    public String getEitherVertex() {
        return source.name;
    }
    
    public String getOtherVertex(String vertex) {
        if (vertex.equals(source.name)) {
            return target.name;
        } else if (vertex.equals(target.name)) {
            return source.name;
        }      
        return "";
    }
    
    public boolean compareEdges(List<Edge> list) {
    	
    	for (Edge e2 : list) {
    		if(source.name.equals(e2.source.name) && target.name.equals(e2.target.name)) {
        		return true;
        	}
        	else if (source.name.equals(e2.target.name) && target.name.equals(e2.source.name)) {
        		return true;
        	}
    	}
    		
    	return false;
    }
    
    @Override
    public int compareTo(Edge o) {
        if (getWeight() < o.getWeight()) {
        return -1;
        } else if (getWeight() > o.getWeight()) {
        return 1;
        }
        return 0;
    }
    
}
