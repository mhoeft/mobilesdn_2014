package net.floodlightcontroller.multicast;

import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class PPH {

    private boolean marked[];
    private Queue<Edge> minSpanningTree;
    private Queue<Edge> priorityQueue;
    private double weight;
    private int numOfEdges[];
    
    public PPH(List<Vertex> vertices, List<Vertex> receivers, Vertex nextVertex) {
        
        marked = new boolean[vertices.size()];
        minSpanningTree = new LinkedList<Edge>();
        priorityQueue = new PriorityQueue<>(vertices.size());
        numOfEdges = new int[vertices.size()];
        
        visit(0, vertices);
        
        while (!priorityQueue.isEmpty()) {
            Edge e = priorityQueue.poll();
            
            String vStr = e.getEitherVertex();
            String wStr = e.getOtherVertex(vStr);
            
            int v = getIndexOfVertex(vStr, vertices);
            int w = getIndexOfVertex(wStr, vertices);
            
            if (marked[v] && marked[w]) {
                continue;
            }
            
            minSpanningTree.add(e);
            
            numOfEdges[v]++;
            numOfEdges[w]++;
            
            weight += e.getWeight();
            if (!marked[v]) {
                visit(v, vertices);
            }
            if (!marked[w]) {
                visit(w, vertices);
            }
        }

        boolean wasOneInNumOfEdges = true;
        
        while (wasOneInNumOfEdges) {
	        wasOneInNumOfEdges = false;
        	for (int i=receivers.size()+1; i<numOfEdges.length; i++) {
	        	if(numOfEdges[i] == 1) {
	        		removeEdgeFromTree(vertices.get(i).name, vertices);
	        		wasOneInNumOfEdges = true;
	        	}
	        }
        }
        
    }
    
    private void removeEdgeFromTree(String verStr, List<Vertex> vertices) {
    	Edge toRemove = new Edge(null, null, 0);
    	for(Edge e : minSpanningTree) {
    		if (e.source.name.equals(verStr) || e.target.name.equals(verStr)) {
    			toRemove = e;
    			break;
    		}
    	}
    	int v1 = getIndexOfVertex(toRemove.source.name, vertices);
    	int v2 = getIndexOfVertex(toRemove.target.name, vertices);
    	numOfEdges[v1]--;
    	numOfEdges[v2]--;
    	minSpanningTree.remove(toRemove);
    }
    
    private void visit(int v, List<Vertex> vertices) {
        marked[v] = true;
        
        for(Edge edge : vertices.get(v).getAdjacencies()) {
        	String tmpStr = edge.getOtherVertex(vertices.get(v).name);
        	int w = getIndexOfVertex(tmpStr, vertices);
            if(!marked[w]) {
                priorityQueue.add(edge);
            }
        }
    }
    
    private int getIndexOfVertex(String nameOfVertex, List<Vertex> vertices) {
    	int index = 0;
    	
    	for(Vertex v : vertices) {
    		if (nameOfVertex.equals(v.name)) {
    			index = vertices.indexOf(v);
    			return index;
    		}
    	}
    	
    	return index;
    }
    
    public Iterable<Edge> edges() {
        return minSpanningTree;
    }

    public double getWeight() {
        return weight;
    }
    

}
