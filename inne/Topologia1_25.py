"""Struktura nr 1

          serw1   serw2
            |       |
            s1      s2
             \     / \
              \   /   \
                s3-----s4-----u2
               /  \
              /    \
             s5-----s6
            /  \    / \ 
           u3   u4 u1 u5

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        serv1 = self.addHost( 'serv1' )
        serv2 = self.addHost( 'serv2' )
        u1 = self.addHost( 'u1' )
        u2 = self.addHost( 'u2' )
        u3 = self.addHost( 'u3' )
        u4 = self.addHost( 'u4' )
        u5 = self.addHost( 'u5' )
	u6 = self.addHost( 'u6' )
        u7 = self.addHost( 'u7' )
        u8 = self.addHost( 'u8' )
        u9 = self.addHost( 'u9' )
        u10 = self.addHost( 'u10' )
	u11 = self.addHost( 'u11' )
        u12 = self.addHost( 'u12' )
        u13 = self.addHost( 'u13' )
        u14 = self.addHost( 'u14' )
        u15 = self.addHost( 'u15' )
	u16 = self.addHost( 'u16' )
        u17 = self.addHost( 'u17' )
        u18 = self.addHost( 'u18' )
        u19 = self.addHost( 'u19' )
        u20 = self.addHost( 'u20' )
	u21 = self.addHost( 'u21' )
        u22 = self.addHost( 'u22' )
        u23 = self.addHost( 'u23' )
        u24 = self.addHost( 'u24' )
        u25 = self.addHost( 'u25' )
        s1 = self.addSwitch ( 's1' )  
        s2 = self.addSwitch ( 's2' )
        s3 = self.addSwitch ( 's3' )
        s4 = self.addSwitch ( 's4' )
        s5 = self.addSwitch ( 's5' )
        s6 = self.addSwitch ( 's6' )

        # Add links

        self.addLink (serv1, s1)
        self.addLink (serv2, s2)
        self.addLink (u1, s6)
        self.addLink (s1, s3)
        self.addLink (s2, s3)
        self.addLink (s2, s4)
        self.addLink (s3, s4)
        self.addLink (s3, s5)
        self.addLink (s3, s6)
        self.addLink (s4, u2)
        self.addLink (s5, s6)
        self.addLink (s5, u3)
        self.addLink (s5, u4)
        self.addLink (s6, u5)

        self.addLink (u6, s2)
        self.addLink (u7, s2)
        self.addLink (u8, s4)
        self.addLink (u9, s4)
        self.addLink (u10, s6)
        self.addLink (u11, s6)
        self.addLink (u12, s6)
        self.addLink (u13, s5)
        self.addLink (u14, s5)
        self.addLink (u15, s5)

	self.addLink (u16, s3)
        self.addLink (u17, s3)
        self.addLink (u18, s1)
        self.addLink (u19, s1)
        self.addLink (u20, s2)
        self.addLink (u21, s4)
        self.addLink (u22, s6)
        self.addLink (u23, s6)
        self.addLink (u24, s5)
        self.addLink (u25, s5)


topos = { 'mytopo': ( lambda: MyTopo() ) }
