#!/bin/bash
# Ten plik trzeba odpalic przez "source ./script.sh" bo nie zadziala (aliasy ustawia sie w subshellu)

# Trzeba to dopisac do pliku ~/.bashrc aby zachowaly sie na stale

DIR=`pwd`
declare -a ALIASES=( \
"alias sudo=\"sudo \"" \
"alias floodlight=\"$DIR/run_floodlight.sh\"" \
"alias diff-multi=\"$DIR/diff-multi.sh\"" \
"alias mininet=\"$DIR/run_mininet.sh\"" )


LEN=${#ALIASES[@]}


if [ "$#" -gt 0 ] ;
then 
    if [ $1 = "perm" ] ; 
    then
        for (( i=0; i<${LEN}; i++ ));
        do
            echo "Adding ${ALIASES[$i]} to .bashrc" 
            echo "${ALIASES[$i]}" >> ~/.bashrc
        done
    fi
else
    for (( i=0; i<${LEN}; i++ ));
    do
        echo "Adding ${ALIASES[$i]}"
        eval "${ALIASES[$i]}"
    done
fi
