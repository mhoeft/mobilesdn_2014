"""Struktura nr 1

          serw1   serw2
            |       |
            s1      s2
             \     / \
              \   /   \
                s3-----s4-----u2
               /  \
              /    \
             s5-----s6
            /  \    / \ 
           u3   u4 u1 u5

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        serv1 = self.addHost( 'serv1' )
        serv2 = self.addHost( 'serv2' )
        u1 = self.addHost( 'u1' )
        u2 = self.addHost( 'u2' )
        u3 = self.addHost( 'u3' )
        u4 = self.addHost( 'u4' )
        u5 = self.addHost( 'u5' )
        s1 = self.addSwitch ( 's1' )  
        s2 = self.addSwitch ( 's2' )
        s3 = self.addSwitch ( 's3' )
        s4 = self.addSwitch ( 's4' )
        s5 = self.addSwitch ( 's5' )
        s6 = self.addSwitch ( 's6' )

        # Add links

        self.addLink (serv1, s1)
        self.addLink (serv2, s2)
        self.addLink (u1, s6)
        self.addLink (s1, s3)
        self.addLink (s2, s3)
        self.addLink (s2, s4)
        self.addLink (s3, s4)
        self.addLink (s3, s5)
        self.addLink (s3, s6)
        self.addLink (s4, u2)
        self.addLink (s5, s6)
        self.addLink (s5, u3)
        self.addLink (s5, u4)
        self.addLink (s6, u5)


topos = { 'mytopo': ( lambda: MyTopo() ) }
