/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dijkstra;
import java.util.PriorityQueue;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Lukas
 */
public class Dijkstra {
    
    public static void computeBestPaths(Vertex source) {
        
        source.minDistance = 0.;
        
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
      	vertexQueue.add(source);

	while(!vertexQueue.isEmpty()) {
            
	    Vertex nextVertex = vertexQueue.poll();

            for(Edge e : nextVertex.adjacencies) {
                
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughNextVertex = nextVertex.minDistance + weight;
                
		if(distanceThroughNextVertex < v.minDistance) {
		    vertexQueue.remove(v);
		    v.minDistance = distanceThroughNextVertex ;
		    v.previous = nextVertex;
		    vertexQueue.add(v);
		}
            }
        }
    }
    
    public static void initVertices(Vertex[] vertices) {
        
        for(Vertex v : vertices) {
            v.previous = null;
            v.minDistance = Double.POSITIVE_INFINITY;
        }    
    }

    public static List<Vertex> getShortestPath(Vertex target) {
        
        List<Vertex> path = new ArrayList<Vertex>();
        
        while(target != null) {
            path.add(target);
            target = target.previous;
        }
        
        Collections.reverse(path);
        return path;
    }

    public static void main(String[] args) {
        
        Vertex v0 = new Vertex("nadawca");
	Vertex v1 = new Vertex("jeden");
	Vertex v2 = new Vertex("dwa");
	Vertex v3 = new Vertex("trzy");
	Vertex v4 = new Vertex("cztery");
        Vertex v5 = new Vertex("piec");
        Vertex v6 = new Vertex("odbiorca 1");
        Vertex v7 = new Vertex("odbiorca 2");
        Vertex v8 = new Vertex("odbiorca 3");

        int receiversCount = 3;
        
	v0.adjacencies = new Edge[]{ new Edge(v0, v1, 1),
	                             new Edge(v0, v2, 1)};
	v1.adjacencies = new Edge[]{ new Edge(v1, v0, 1),
                                     new Edge(v1, v3, 1)};
	v2.adjacencies = new Edge[]{ new Edge(v2, v0, 1),
                                     new Edge(v2, v4, 1),
                                     new Edge(v2, v5, 1)};
	v3.adjacencies = new Edge[]{ new Edge(v3, v1, 1),
	                             new Edge(v3, v4, 1),
                                     new Edge(v3, v6, 1) };
	v4.adjacencies = new Edge[]{ new Edge(v4, v2, 1),
                                     new Edge(v4, v3, 1),
                                     new Edge(v4, v5, 1) };
        v5.adjacencies = new Edge[]{ new Edge(v5, v2, 1),
                                     new Edge(v5, v4, 1),
                                     new Edge(v5, v7, 1),
                                     new Edge(v5, v8, 1) };
        v6.adjacencies = new Edge[]{ new Edge(v6, v3, 1)};
        v7.adjacencies = new Edge[]{ new Edge(v7, v5, 1)};
        v8.adjacencies = new Edge[]{ new Edge(v8, v5, 1)};
        
        Vertex[] vertices = { v0, v1, v2, v3, v4, v5, v6, v7, v8 };
        Vertex[] receivers = { v6, v7, v8 };
        Vertex nextVertex = v0;
        Vertex bestVertex = new Vertex();      
        List<List<Vertex>> multicast = new ArrayList<List<Vertex>>();
        
        for(int i=0; i<receiversCount; i++) {          
            bestVertex.minDistance = 100;
            computeBestPaths(nextVertex);
            for(Vertex v : receivers) {
                if(v.belongToSteiner == false && v.minDistance < bestVertex.minDistance) {
                    nextVertex = v;
                    bestVertex.minDistance = v.minDistance;
                }
            }
            nextVertex.belongToSteiner = true;
            multicast.add(getShortestPath(nextVertex));
            initVertices(vertices);
        }
        
        for (List<Vertex> lista : multicast) {
            System.out.println("Path number: " + multicast.indexOf(lista));
            System.out.println(lista);        
        }
    }
}
