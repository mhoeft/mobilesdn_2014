/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dijkstra;

/**
 *
 * @author Lukas
 */
public class Edge {
    
    public Vertex source;
    public Vertex target;
    public double weight;
    
    public Edge(Vertex argSource, Vertex argTarget, double argWeight) { 
        source = argSource;
        target = argTarget;
        weight = argWeight;
    }

    public Vertex getSource() {
        return source;
    }

    public void setSource(Vertex source) {
        this.source = source;
    }

    public Vertex getTarget() {
        return target;
    }

    public void setTarget(Vertex target) {
        this.target = target;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    
}
