/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dijkstra;

/**
 *
 * @author Lukas
 */
public class Vertex implements Comparable<Vertex> {
    
    public String name;
    public Edge[] adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public boolean belongToSteiner = false;
    
    public Vertex() {
    }
    
    public Vertex(String argName) {
        name = argName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Edge[] getAdjacencies() {
        return adjacencies;
    }

    public void setAdjacencies(Edge[] adjacencies) {
        this.adjacencies = adjacencies;
    }

    public double getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }

    public Vertex getPrevious() {
        return previous;
    }

    public void setPrevious(Vertex previous) {
        this.previous = previous;
    }

    public boolean isBelongToSteiner() {
        return belongToSteiner;
    }

    public void setBelongToSteiner(boolean belongToSteiner) {
        this.belongToSteiner = belongToSteiner;
    }
 
    @Override
    public String toString() {
        return name;
    }
    
    @Override
    public int compareTo(Vertex other) {
        return Double.compare(minDistance, other.minDistance);
    }
}
