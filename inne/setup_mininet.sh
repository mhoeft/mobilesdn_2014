  #mininet
  #floodlight
  #ip link add name komp-s1 type veth peer name s1-komp
  #ip link add name komp-s2 type veth peer name s2-komp
  #ip link add name komp-s5 type veth peer name s5-komp
  #ip link add name komp-s6 type veth peer name s6-komp
  #ip a add 10.0.0.101/8 dev komp-s1
  #ip a add 10.0.0.102/8 dev komp-s2
  #ip a add 10.0.0.105/8 dev komp-s5
  #ip a add 10.0.0.106/8 dev komp-s6
  #ovs-vsctl add-port s1 s1-komp
  #ovs-vsctl add-port s2 s2-komp
  #ovs-vsctl add-port s5 s5-komp
  #ovs-vsctl add-port s6 s6-komp
  #ip link set s1-komp up
  #ip link set s2-komp up
  #ip link set s5-komp up
  #ip link set s6-komp up
  #ip link set komp-s1 up
  #ip link set komp-s2 up
  #ip link set komp-s5 up
  #ip link set komp-s6 up
  #ip rule add from 10.1.0.0/16 lookup serv1
  #ip rule add from 10.2.0.0/16 lookup serv2
  #ip rule add from 10.3.0.0/16 lookup ap1
  #ip rule add from 10.4.0.0/16 lookup ap2
  #ip route add 10.0.0.0/16 dev komp-s1
  #ip route add 10.0.0.0/16 dev komp-s1 table serv1
  #ip route add 10.0.0.0/16 dev komp-s2 table serv2
  #ip route add 10.0.0.0/16 dev komp-s5 table ap1
  #ip route add 10.0.0.0/16 dev komp-s6 table ap2
  #echo 1 > /proc/sys/net/ipv4/conf/eth0/proxy_arp
  #echo 1 > /proc/sys/net/ipv4/conf/komp-s1/proxy_arp
  #echo 1 > /proc/sys/net/ipv4/conf/komp-s2/proxy_arp
  #echo 1 > /proc/sys/net/ipv4/conf/komp-s5/proxy_arp
  #echo 1 > /proc/sys/net/ipv4/conf/komp-s6/proxy_arp
  #echo 1 > /proc/sys/net/ipv4/conf/s1-komp/proxy_arp
  #echo 1 > /proc/sys/net/ipv4/conf/s2-komp/proxy_arp
  #echo 1 > /proc/sys/net/ipv4/conf/s5-komp/proxy_arp
  #echo 1 > /proc/sys/net/ipv4/conf/s6-komp/proxy_arp
  ip link add name komp-s1 type veth peer name s1-komp
  ip a add 10.5.1.1/8 dev komp-s1
  ip link set dev komp-s1 up
  ip link set dev s1-komp up
  ovs-vsctl add-port s1 s1-komp
  ip a fl eth0
  ip a add 192.168.99.5/24 dev eth0
  vconfig add eth0 101
  vconfig add eth0 102
  vconfig add eth0 103 
  vconfig add eth0 104
  ifconfig eth0.101 up
  ifconfig eth0.102 up
  ifconfig eth0.103 up
  ifconfig eth0.104 up
  ovs-vsctl add-port s1 eth0.101
  ovs-vsctl add-port s2 eth0.102
  ovs-vsctl add-port s5 eth0.103
  ovs-vsctl add-port s6 eth0.104 
  echo 1 > /proc/sys/net/ipv4/conf/eth0/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/eth0.101/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/eth0.102/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/eth0.103/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/eth0.104/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/s1-komp/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/komp-s1/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/s1/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/s2/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/s3/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/s4/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/s5/proxy_arp
  echo 1 > /proc/sys/net/ipv4/conf/s6/proxy_arp

  # vconfig + ip
  # ovs-vsctl set-controller br-int tcp:192.168.99.5:6633
  # ovs-vsctl del-port br-int p6p1
  # ovs-vsctl add-port br-int p6p1.50

# NAT (internet dla hostow, opcjonalne - wymaga dodania na kazdym hoscie default gateway = 10.5.1.1):
iptables -F
#iptables -t nat -A POSTROUTING -o eth2 -j MASQUERADE




