# Narzedzie do rekursyjnego diffowania plikow tekstowych
# Mozna uzyc tez komendy diff -r, ale komenda ta nie znajduje plikow
# zaczynajacych sie od kropki i porownuje takze pliki binarne

# Warto uzyc komendy "alias diff-multi=<sciezka_do_tego_skryptu"zeby moc szybko uzywac skryptu

# TODO: informowanie, ze plik znajduje sie tylko w jednym z folderow. Zawsze mozna po prostu usunac "2>/dev/null" w linijkach korzystajacych z diffa.
function listTextFiles {
    find "$1" -type f -exec file {} \; | grep text | cut -d ':' -f1
}


if [ ! -d $1 ]
then
    echo "Katalog $1 nie istnieje"
elif [ ! -d $2 ] 
then
    echo "Katalog $2 nie istnieje"
else
	ORIGIN=`pwd`
	cd $1
	FILES=`listTextFiles .`
	cd $ORIGIN
	for file in $FILES; do
	    FILE2="$2/$file"
	    RESULT=`diff "$1/$file" "$FILE2" 2> /dev/null`
	    if [ -n "$RESULT" ]
	    then
	        echo "=============================="
	        echo "$1/$file vs $FILE2"
	        diff "$1/$file" "$FILE2" 2> /dev/null
	        echo "=============================="
	        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	     fi
	done
fi
