#!/bin/bash

# SDN Interface 
IFC=p20p1

# Internet interface
INTERNET=p56p1

echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -t nat -A POSTROUTING -o $INTERNET -s 10.1.1.0/24 -j MASQUERADE

systemctl start  openvswitch.service
sleep 3
systemctl status openvswitch.service 

ovs-vsctl add-port br-int $IFC
ip a fl dev $IFC
ifconfig br-int 10.1.1.16/24 up
