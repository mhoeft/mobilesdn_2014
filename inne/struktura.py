"""Struktura nr 1

     u1----s1      s2
             \     / \
              \   /   \
                s3-----s4-----u2
               /  \
              /    \
             s5-----s6
            /  \      \ 
           u3   u4     u5

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        u1 = self.addHost( 'u1' )
        u2 = self.addHost( 'u2' )
        u3 = self.addHost( 'u3' )
        u4 = self.addHost( 'u4' )
        u5 = self.addHost( 'u5' )
        s1 = self.addSwitch ( 's1' )  
        s2 = self.addSwitch ( 's2' )
        s3 = self.addSwitch ( 's3' )
        s4 = self.addSwitch ( 's4' )
        s5 = self.addSwitch ( 's5' )
        s6 = self.addSwitch ( 's6' )

        # Add links

        self.addLink (u1, s1)
        self.addLink (s1, s3)
        self.addLink (s2, s3)
        self.addLink (s2, s4)
        self.addLink (s3, s4)
        self.addLink (s3, s5)
        self.addLink (s3, s6)
        self.addLink (s4, u2)
        self.addLink (s5, s6)
        self.addLink (s5, u3)
        self.addLink (s5, u4)
        self.addLink (s6, u5)
	
	# To nie dziala, hm.
	#serv1.setIP('10.0.0.1',16)
	#serv2.setIP('10.0.0.2',16)

	#s1.setIP('10.0.1.1',16)
	#s2.setIP('10.0.1.2',16)
	#s3.setIP('10.0.1.3',16)
	#s4.setIP('10.0.1.4',16)
	#s5.setIP('10.0.1.5',16)
	#s6.setIP('10.0.1.6',16)

	#u1.setIP('10.0.2.1',16)
	#u2.setIP('10.0.2.2',16)
	#u3.setIP('10.0.2.3',16)
	#u4.setIP('10.0.2.4',16)
	#u5.setIP('10.0.2.5',16)
	

topos = { 'mytopo': ( lambda: MyTopo() ) }
