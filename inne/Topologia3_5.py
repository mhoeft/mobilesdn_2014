"""Struktura nr 1

          serw1   serw2
            |       |
            s1      s2
             \     / \
              \   /   \
                s3-----s4-----u2
               /  \
              /    \
             s5-----s6
            /  \    / \ 
           u3   u4 u1 u5

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        serv1 = self.addHost( 'serv1' )
        serv2 = self.addHost( 'serv2' )
        u1 = self.addHost( 'u1' )
        u2 = self.addHost( 'u2' )
        u3 = self.addHost( 'u3' )
        u4 = self.addHost( 'u4' )
        u5 = self.addHost( 'u5' )
        s1 = self.addSwitch ( 's1' )  
        s2 = self.addSwitch ( 's2' )
        s3 = self.addSwitch ( 's3' )
        s4 = self.addSwitch ( 's4' )
        s5 = self.addSwitch ( 's5' )
        s6 = self.addSwitch ( 's6' )
	s7 = self.addSwitch ( 's7' )  
        s8 = self.addSwitch ( 's8' )
        s9 = self.addSwitch ( 's9' )
        s10 = self.addSwitch ( 's10' )
        s11 = self.addSwitch ( 's11' )
        s12 = self.addSwitch ( 's12' )
	s13 = self.addSwitch ( 's13' )
        s14 = self.addSwitch ( 's14' )
        s15 = self.addSwitch ( 's15' )
        s16 = self.addSwitch ( 's16' )
	s17 = self.addSwitch ( 's17' )  
        s18 = self.addSwitch ( 's18' )
        s19 = self.addSwitch ( 's19' )
        s20 = self.addSwitch ( 's20' )

        # Add links

        self.addLink (serv1, s1)
        self.addLink (serv2, s2)
        self.addLink (s1, s3)
        self.addLink (s2, s3)
        self.addLink (s2, s4)
        self.addLink (s3, s4)
        self.addLink (s3, s5)
        self.addLink (s3, s6)
        self.addLink (s5, s6)

        self.addLink (s2, s7)
        self.addLink (s7, s4)
        self.addLink (s7, s8)
        self.addLink (s9, s8)
        self.addLink (s8, s12)
        self.addLink (s9, s4)
        self.addLink (s3, s10)
        self.addLink (s5, s10)
        self.addLink (s5, s11)
	self.addLink (s4, s6)

	self.addLink (s13, s11)
        self.addLink (s14, s5)
        self.addLink (s15, s5)
        self.addLink (s16, s6)
        self.addLink (s17, s9)
        self.addLink (s18, s9)
        self.addLink (s19, s8)
        self.addLink (s20, s12)

	self.addLink (u1, s14)
        self.addLink (u2, s15)
        self.addLink (u3, s16)
        self.addLink (u4, s18)
        self.addLink (u5, s19)


topos = { 'mytopo': ( lambda: MyTopo() ) }
