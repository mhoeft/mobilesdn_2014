#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QAction>
#include <QDebug>
#include <QUdpSocket>
#include <QHostAddress>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{   
    ui->setupUi(this);

    QAction *act = new QAction(this);
    act->setShortcut(QKeySequence("Ctrl+m"));
    act->setCheckable(true);
    addAction(act);
    connect(act, SIGNAL(triggered(bool)), ui->gstBox, SLOT(setVisible(bool)));
    QAction *actRun = new QAction(this);
    actRun->setShortcut(QKeySequence("Ctrl+r"));
    addAction(actRun);
    connect(actRun, SIGNAL(triggered()), ui->button, SLOT(click()));
    QAction *actQuit = new QAction(this);
    actQuit->setShortcut(QKeySequence("Ctrl+q"));
    addAction(actQuit);
    connect(actQuit, SIGNAL(triggered()), qApp, SLOT(quit()));

    ui->gstBox->setVisible(false);

    connect(ui->button, SIGNAL(clicked()), SLOT(doJoin()));

    proc = 0;
}

MainWindow::~MainWindow()
{
    if (proc) {
        proc->kill();
        proc->waitForFinished();
    }

    delete ui;
}

void MainWindow::act()
{
    qDebug() << "bla";
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    /* Send "register" packet */
    QUdpSocket sock(this);
    QHostAddress serv(ui->ipAddr->text());

    if (serv.isNull() || serv.toString() != ui->ipAddr->text()) {
        qDebug() << "Wrong IP";
        return;
    }

    sock.connectToHost(serv, 60000);
    sock.waitForConnected();

    QByteArray datagram("Hello!");
    sock.write(datagram);
    sock.close();

    event->accept();
}

void MainWindow::doJoin()
{
    /* Validate IP address and start periodic register */
    QHostAddress serv(ui->ipAddr->text());

    if (serv.isNull() || serv.toString() != ui->ipAddr->text()) {
        qDebug() << "Wrong IP";
        return;
    }
    timerId = startTimer(2000);

    /* Reconfigure UI */
    ui->button->setText("Leave");
    ui->port->setEnabled(false);
    ui->ipAddr->setEnabled(false);

    disconnect(ui->button, SIGNAL(clicked()), this, SLOT(doJoin()));
    connect(ui->button, SIGNAL(clicked()), SLOT(doLeave()));

    /* Start the player process */
    QString program = ui->gstBin->text();
    QStringList params;
    //params << "udpsrc" << "port=" + ui->port->text()
    //       << "caps=application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, sprop-parameter-sets=(string)\"Z01AFeygoP2AiAAAAwALuaygAHixbLA\\=\\,aOvssg\\=\\=\", payload=(int)96, ssrc=(uint)1111162409, clock-base=(uint)899166813, seqnum-base=(uint)5111"
    //       << "!" << "rtph264depay" << "!" << "ffdec_h264" << "!" << "xvimagesink" << "sync=false";
    params = ui->gstConf->toPlainText().split(" ");
    params.replaceInStrings("%caps%", ui->gstCaps->toPlainText());

    proc = new QProcess(this);
    proc->start(program, params);
    qDebug() << proc->waitForStarted();
    qDebug() << proc->errorString() << proc->error();
    qDebug() << proc->readAll() << proc->exitCode();
}

void MainWindow::doLeave()
{
    ui->button->setText("Join!");
    ui->port->setEnabled(true);
    ui->ipAddr->setEnabled(true);

    killTimer(timerId);

    disconnect(ui->button, SIGNAL(clicked()), this, SLOT(doLeave()));
    connect(ui->button, SIGNAL(clicked()), SLOT(doJoin()));

    if (proc) {
        proc->kill();
        proc->waitForFinished();
        proc = 0;
    }
}
