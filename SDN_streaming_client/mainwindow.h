#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void timerEvent(QTimerEvent *event);

private slots:
    void doJoin();
    void doLeave();
    void act();

private:
    Ui::MainWindow *ui;
    QProcess *proc;
    int timerId;
};

#endif // MAINWINDOW_H
